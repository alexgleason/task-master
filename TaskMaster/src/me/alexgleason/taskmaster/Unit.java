package me.alexgleason.taskmaster;

import java.util.Date;

/*
 * 
 * @author Derek Argueta
 * 
 * This object is the task/project the user is adding, with certain features such as
 * name, due date, children tasks, etc.
 */

public class Unit {
	private Long id;
	private String name; // name of this item
	private boolean important; // importance: 1/true = important
	private UnitList immediateChildren; // list of child tasks/projects
	private Date dueDate; // the date this is due
	private double estimatedTime; // time for this to be completed (in hours)

	// Constructor
	public Unit(String n, boolean i, Date d, double et) {
		name = n;
		important = i;
		immediateChildren = new UnitList();
		dueDate = d;
		estimatedTime = et;
	}

	/******** C H I L D A D D / D E L E T E M E T H O D S *************/
	/*
	 * Add a new child task/project to this item
	 * 
	 * @param the new item to be added in
	 */
	public void addChild(Unit x) {
		immediateChildren.add(x);
	}

	/*
	 * Remove a child task/project from this item
	 * 
	 * @param the item object to be removed from this list
	 */
	public void removeChild(Unit x) {
		immediateChildren.remove(immediateChildren.indexOf(x));
	}

	/******** S E T T E R M E T H O D S *************/
	/*
	 * @param the new name for this item used for editing this item
	 */
	public void setName(String n) {
		name = n;
	}

	/*
	 * @param the new importance boolean for this item 1/true = important
	 * 0/false = unimportant used for editing this item
	 */
	public void setImportance(boolean i) {
		important = i;
	}

	/*
	 * @param the new due date of this item used for editing this item
	 */
	public void setDueDate(Date d) {
		dueDate = d;
	}

	/*
	 * @param the new estimated time to complete this item used for editing this
	 * item
	 */
	public void setEstimatedTime(int et) {
		estimatedTime = et;
	}

	/******** G E T T E R M E T H O D S *************/
	/*
	 * @returns the ID
	 */
	public Long getId() {
		return id;
	}

	/*
	 * @returns the name of this task/project/goal thingy
	 */
	public String getName() {
		return name;
	}

	/*
	 * @returns the boolean value the user entered to indicate if this item is
	 * important or not
	 */
	public boolean getImportance() {
		return important;
	}

	/*
	 * @returns an arraylist of the immediate children tasks Do note that the
	 * children in this list could have children of their own
	 */
	public UnitList getChildren() {
		return immediateChildren;
	}

	/*
	 * @returns the Date object representation of when this item is due. If
	 * null, then that means the user never entered anything
	 */
	public Date getDueDate() {
		return dueDate;
	}

	/*
	 * @returns the amount of time the user estimated it will take to complete
	 * this task this being a double, .5 = 30 minutes, .75 = 45 minutes etc.
	 */
	public double getEstimatedTime() {
		return estimatedTime;
	}
}
