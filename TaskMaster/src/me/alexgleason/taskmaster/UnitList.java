package me.alexgleason.taskmaster;

import java.text.*;
import java.util.*;
import java.lang.Math;

/*
 * @author Derek Argueta
 * 
 * This object is a container which will hold a full list of the user's tasks/project.
 */
public class UnitList extends ArrayList<Unit> {
	private static final long serialVersionUID = 1L;

	// constructor
	public UnitList() {
		super();
	}

	// Gets the total time of all children units
	// @author Alex Gleason
	public double getTotalTime() {
		double result = 0.0;
		for (Unit unit : this) {
			result += unit.getEstimatedTime();
		}
		return result;
	}
	
	//sorts the list by the eisenhower matrix
	public void sortEisenhower(){
		ArrayList<Unit> important = new ArrayList<Unit>();
		ArrayList<Unit> unimportant = new ArrayList<Unit>();
		for (Unit unit : this) {
			if(unit.getImportance()==true){ //put all the important tasks in one list
				important.add(unit);
			} else{ //put all the unimportant tasks in the other list
				unimportant.add(unit);
			}
		}
		for(int x=0;x<important.size();x++){
			for(int y=0;y<important.size()-1;y++){
				if(ratio(important.get(y))<ratio(important.get(y+1))){
					Unit temp=important.get(y);
					important.set(y,important.get(y+1));
					important.set(y+1,temp);
				}
			}
		}	for(int x=0;x<unimportant.size();x++){
			for(int y=0;y<unimportant.size()-1;y++){
				if(ratio(unimportant.get(y))<ratio(unimportant.get(y+1))){
					Unit temp=unimportant.get(y);
					unimportant.set(y,unimportant.get(y+1));
					unimportant.set(y+1,temp);
				}
			}
		}
		important.addAll(unimportant);
		for(int x = 0; x < this.size(); x++){
			this.set(x, important.get(x));
		}
	
	}
	
	//returns the ration of estimated completion time over time left
	public double ratio(Unit unit){
		//Get the current date in string format
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd/HH/mm/ss", Locale.ENGLISH);
		String current = df.format(new Date());
		String due = df.format(unit.getDueDate());
		double difference = ( getDateVal(due) - getDateVal(current) ); //assuming the due date has not passed, due should be larger
		return unit.getEstimatedTime()/Math.pow(difference, 2); //estimated time over time left
	}
	
	//return a double value for the date
	public double getDateVal(String c){
		double result = 0;
		String[] current = c.split("/");
		result += (Double.parseDouble(current[0])*365); //add years into days
		result += (getMonth(Integer.parseInt(current[1]), Integer.parseInt(current[0]))); //add month in days
		result += Double.parseDouble(current[2]); // add days
		result += Double.parseDouble(current[3]) / 24; //add hours in days
		result += Double.parseDouble(current[4]) / 1440; //add minutes in days
		result += Double.parseDouble(current[5]) / 86400; //add seconds in days
		return result;
	}
	
	//return the number of days in a month
	public int getMonth(int m, int y){
		if (m == 4 || m == 6 || m == 9 || m == 11){
			return 30;
		}else if (m == 2){
			if(y % 4 == 0){ //leap year
				return 29;
			} else {
				return 28;
			} 
		} else {
			return 31;
		}
	}
	
}