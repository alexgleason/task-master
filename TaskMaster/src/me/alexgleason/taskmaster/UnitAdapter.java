package me.alexgleason.taskmaster;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class UnitAdapter extends BaseAdapter {
	private static UnitList unitList;
	private LayoutInflater mInflater;

	public UnitAdapter(Context context, UnitList unitList) {
		UnitAdapter.unitList = unitList;
		mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return unitList.size();
	}

	@Override
	public Object getItem(int position) {
		return unitList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return unitList.get(position).getId().longValue();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (null == convertView) {
			Unit unit = unitList.get(position);
			//Get the current date in string format
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd/HH/mm/ss", Locale.ENGLISH);
			String[] current = df.format(new Date()).split("/");
			String[] unitDate = df.format(unit.getDueDate()).split("/");
			String result="Due in ";
			if(Integer.parseInt(unitDate[0]) - Integer.parseInt(current[0]) > 0){
				result += (Integer.parseInt(unitDate[0]) - Integer.parseInt(current[0]));
				if(Integer.parseInt(unitDate[0]) - Integer.parseInt(current[0]) == 1)
					result += " year";
				else
					result += " years";
			} else if(Integer.parseInt(unitDate[1]) - Integer.parseInt(current[1]) > 0){
				result += (Integer.parseInt(unitDate[1]) - Integer.parseInt(current[1]));
				if(Integer.parseInt(unitDate[1]) - Integer.parseInt(current[1]) == 1)
					result += " month";
				else
					result += " months";
			} else if(Integer.parseInt(unitDate[2]) - Integer.parseInt(current[2]) > 0){
				result += (Integer.parseInt(unitDate[2]) - Integer.parseInt(current[2]));
				if(Integer.parseInt(unitDate[2]) - Integer.parseInt(current[2]) == 1)
					result += " day";
				else
					result += " days";
			} else if(Integer.parseInt(unitDate[3]) - Integer.parseInt(current[3]) > 0){
				result += (Integer.parseInt(unitDate[3]) - Integer.parseInt(current[3]));
				if(Integer.parseInt(unitDate[3]) - Integer.parseInt(current[3]) == 1)
					result += " hour";
				else
					result += "hours";
			} else if(Integer.parseInt(unitDate[4]) - Integer.parseInt(current[4]) > 0){
				result += (Integer.parseInt(unitDate[4]) - Integer.parseInt(current[4]));
				if(Integer.parseInt(unitDate[4]) - Integer.parseInt(current[4]) == 1)
					result += " minute";
				else
					result += " minutes";
			} else {
				result += (Integer.parseInt(unitDate[5]) - Integer.parseInt(current[5]));
				if(Integer.parseInt(unitDate[5]) - Integer.parseInt(current[5]) == 1)
					result += " second";
				else
					result += " seconds";
			}
			
			convertView = mInflater.inflate(R.layout.unit_row_view, null);
			holder = new ViewHolder();
			holder.name = (TextView) convertView.findViewById(R.id.unit_row_view_name);
			holder.name.setText(unit.getName()+"     ["+result+"]");
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		return convertView;
	}

	static class ViewHolder {
		TextView name;
	}

}
