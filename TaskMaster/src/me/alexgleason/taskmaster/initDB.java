package me.alexgleason.taskmaster;

import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.*;

public class initDB {
	public static final String TABLE_ID = "_id";
	public static final String TABLE_NAME = "name";
	public static final String TABLE_IMP = "important";
	public static final String TABLE_END = "end";
	public static final String TABLE_EST = "estTime";
	public static final String TABLE_PARENT = "parent";

	private static final String DATABASE_NAME = "tasks";
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_CREATE = "create table "
			+ DATABASE_NAME + "(" + TABLE_ID
			+ " integer primary key autoincrement, " + TABLE_NAME
			+ " text not null, " + TABLE_IMP + " integer, " + TABLE_END
			+ " text, " + TABLE_EST + " text, " + TABLE_PARENT + " integer);";

	private final Context context;
	private dbAssist assister;
	private SQLiteDatabase db;

	public initDB(Context c) {
		this.context = c;
		assister = new dbAssist(context);
	}

	private static class dbAssist extends SQLiteOpenHelper {
		dbAssist(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
		}
	}

	public initDB open() throws SQLException {
		db = assister.getWritableDatabase();
		return this;
	}

	public void close() {
		assister.close();
	}

	public long insertUnit(Unit unit) {
		ContentValues values = new ContentValues();
		values.put(TABLE_NAME, unit.getName());
		values.put(TABLE_EST, unit.getEstimatedTime());
		values.put(TABLE_END, unit.getDueDate().toString());
		values.put(TABLE_IMP, unit.getImportance());
		return db.insert(DATABASE_NAME, null, values);
	}

	public UnitList getAllUnits() {
		UnitList g=new UnitList();
		Cursor t= db
				.query(DATABASE_NAME, new String[] { TABLE_ID, TABLE_NAME,
						TABLE_EST, TABLE_END, TABLE_IMP }, null, null, null,
						null, null);
		if(t.moveToFirst()){
			do{
			   	Unit k=new Unit(null, false, null ,0.0);
			   	k.setName(t.getString(t
                        .getColumnIndex(TABLE_NAME)));
				k.setImportance((t.getString(t
                        .getColumnIndex(TABLE_IMP))).equalsIgnoreCase("true")?true:false);
				k.setEstimatedTime(Integer.parseInt(t.getString(t
                        .getColumnIndex(TABLE_END))));
				String q[]=(t.getString(t
                        .getColumnIndex(TABLE_END))).split("[-]");
				Date l=new Date();
				l.setMonth(Integer.parseInt(q[0])-1);
				l.setYear(Integer.parseInt(q[2])-1900);
				l.setDate(Integer.parseInt(q[1]));
				k.setDueDate(l);
			   	g.add(k);
			}while(t.moveToNext());
		}
		return g;
	}

}