package me.alexgleason.taskmaster;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.*;
import android.widget.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ListDisplayActivity extends Activity {

	ListView listView1;
	UnitAdapter unitAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState){//throws SQL Exception {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_display);

		listView1 = (ListView) findViewById(R.id.listView1);
		listView1.setAdapter(new UnitAdapter(this, (new initDB(this)).open().getAllUnits()));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list_display, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.main_add_unit_button:
			startActivity(new Intent(this, AddUnitActivity.class));
			return true;
		}
		return false;
	}

}
