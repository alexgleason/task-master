package me.alexgleason.taskmaster;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class AddUnitActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_unit);
	
	Button button = (Button)findViewById(R.id.save_button);
	button.setOnClickListener(new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			try {
				saveUnit();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	});
	}




	private void saveUnit() throws ParseException {
		Unit unit = makeUnit();
		
		
		// Add unit to database here
		new initDB(this).insertUnit(unit);
		finish();
	}

	private Unit makeUnit() throws ParseException {
		String name = ((EditText) findViewById(R.id.activity_add_unit_name))
				.getText().toString();
		Log.d("rahul", "UNIT NAME: "+name);
		boolean important = ((CheckBox) findViewById(R.id.activity_add_unit_important))
				.isChecked();
		Log.d("rahul", "UNIT IMPORTANCE: " + important);
		Date date = new SimpleDateFormat("MM-d-yyyy", Locale.ENGLISH)
				.parse(((EditText) findViewById(R.id.activity_add_unit_date))
						.getText().toString());
		Log.d("rahul", "UNIT DUE DATE: " + date.toString());
		double time = Double
				.parseDouble(((EditText) findViewById(R.id.activity_add_unit_time))
						.getText().toString());
		Log.d("rahul", "UNIT ESTIMATED TIME: " + time);
		return new Unit(name, important, date, time);
	}
}
